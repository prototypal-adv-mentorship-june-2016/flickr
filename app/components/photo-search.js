import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

export default Ember.Component.extend({
  flickr: Ember.inject.service(),
  locationService: Ember.inject.service('location'),
  hasSetupLiveSearch: false,
  photos: null,
  terms: null,
  search: task(function * () {
    if (Ember.isEmpty(this.get('terms'))) {
      return null;
    }
    this._setUpLiveSearchIfFirstSearch();
    let results = yield this.get('flickr').search(this.get('terms'), this.get('locationService.currentLocation'));
    this.set('photos', results);
    return results;
  }).restartable(),

  hasSearched: Ember.computed('search.lastSuccessful.value', function() {
    return (!!this.get('search.lastSuccessful.value'));
  }),

  triggerSearch: task(function *(event) {
    let terms = event.target.value;
    yield timeout(500);
    this.set('terms', terms);
    yield this.get('search').perform();
  }).restartable(),

  _setUpLiveSearchIfFirstSearch() {
    if (!this.get('hasSetupLiveSearch')) {
      this.get('locationService').watchPosition(() => {
        if(!this.isDestroyed) {
          this.get('search').perform();
        }
      });
      this.set('hasSetupLiveSearch', true);
    }
  }
});
