import Ember from 'ember';

export default Ember.Route.extend({
  flickr: Ember.inject.service(),

  model(params) {
    return this.get('flickr').findPhoto(params.photoId);
  }
});
