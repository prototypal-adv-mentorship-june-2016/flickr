import Ember from 'ember';
import $ from 'jquery';

export default Ember.Service.extend({
  init() {
    this._super(...arguments);
    this._photoCache = {};
  },

  reset() {
    this._photoCache = {};
  },

  search(searchTerm, location) {
    let searchOptions = {};

    if (!Ember.isEmpty(searchTerm)) {
      searchOptions.tags = searchTerm;
    }

    if (!Ember.isEmpty(location)) {
      searchOptions.lat = location.latitude;
      searchOptions.lon = location.longitude;
    }

    return this.apiCall('flickr.photos.search', searchOptions).then(res => {
      let photos = res.photos.photo;
      photos.forEach(photo => {
        this._photoCache[photo.id] = photo;
      });
      return photos;
    });
  },
  findPhoto(photoId) {
    let cachedPhoto = this._photoCache[photoId];
    if (cachedPhoto) {
      return Ember.RSVP.resolve(cachedPhoto);
    } else {
      return this.apiCall('flickr.photos.getInfo', {photo_id: photoId}).then(res => { return res.photo; });
    }
  },
  apiCall(method, params) {
    let apiKey = '18f07afc15c9c8755e4394e0c9cdbf32';
    let url = `https://api.flickr.com/services/rest/?method=${method}&api_key=${apiKey}&format=json&nojsoncallback=1`;
    url += '&' + $.param(params);
    return Ember.RSVP.resolve($.getJSON(url));
  }
});
