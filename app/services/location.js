import Ember from 'ember';

export default Ember.Service.extend({
  isAvailable: false,
  _watchId: null,
  init() {
    this._detectAvailability();
    this.watchPosition();
  },
  _detectAvailability() {
    if ("geolocation" in navigator) {
      this.set('isAvailable', true);
    } else {
      this.set('isAvailable', false);
    }
  },
  _clearWatchIfNecessary() {
    if (this.get('_watchId')) {
      navigator.geolocation.clearWatch(this.get('_watchId'));
    }
  },
  willDestroy() {
    this._clearWatchIfNecessary();
  },
  watchPosition(callback) {
    if(!this.get('isAvailable')) { return; }
    this._clearWatchIfNecessary();
    let watchId = navigator.geolocation.watchPosition((geoObject) => {
      Ember.run(() => {
        this._updateCurrentLocation(geoObject);
        if (callback) {
          callback();
        }
      });
    }, (reason) => {
      console.error(reason);
    });
    this.set('_watchId', watchId);
  },
  _updateCurrentLocation(geo) {
    this.set('_currentLocation', geo);
  },
  currentLocation: Ember.computed('_currentLocation', function() {
    let _currentLocation = this.get('_currentLocation');
    if (!Ember.isEmpty(_currentLocation)) {
      return Ember.Object.create({
        latitude: _currentLocation.coords.latitude,
        longitude: _currentLocation.coords.longitude
      });
    }
  })
});
