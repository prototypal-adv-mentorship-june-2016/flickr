import Ember from 'ember';

export function formatLocation([geo]/*, hash*/) {
  if (!Ember.isEmpty(geo)) {
    return `${geo.latitude}, ${geo.longitude}`;
  }
}

export default Ember.Helper.helper(formatLocation);
