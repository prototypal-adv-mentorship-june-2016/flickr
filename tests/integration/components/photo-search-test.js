import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import wait from 'ember-test-helpers/wait';
import hbs from 'htmlbars-inline-precompile';

const fakeFlickr = Ember.Service.extend({
  search() {
    return Ember.RSVP.reject();
  }
});

const fakeLocation = Ember.Service.extend({
  currentLocation() {
  },
  watchPosition() {
  }
});

moduleForComponent('photo-search', 'Integration | Component | photo search', {
  integration: true,
  beforeEach() {
    this.register('service:flickr', fakeFlickr);
    this.inject.service('flickr', { as: 'flickrStub' });

    this.register('service:location', fakeLocation);
    this.inject.service('location', { as: 'locationServiceStub' });
  }
});

test('it yields nothing when have not searched', function(assert) {
  assert.expect(1);
  let done = assert.async();

  let originalSearch = this.get('flickrStub').search;
  this.get('flickrStub').search = () => {
    assert.ok(false, 'Search should not be called');
    return Ember.RSVP.resolve([1,2,3]);
  };

  this.render(hbs`
    {{#photo-search as |searchResults|}}
      {{#each searchResults as |result|}}
        <div class="test-result"></div>
      {{/each}}
    {{/photo-search}}
  `);

  wait().then(() => {
    assert.equal(this.$('.test-result').length, 0, 'No results show');
    this.get('flickrStub').search = originalSearch;
    done();
  });
});

test('it searches as you type', function(assert) {
  assert.expect(2);

  let done = assert.async();

  let searchCount = 0;
  let originalSearch = this.get('flickrStub').search;
  this.get('flickrStub').search = () => {
    searchCount++;
    return Ember.RSVP.resolve([1,2,3]);
  };

  let originalWatchPosition = this.get('locationServiceStub').watchPosition;
  this.get('locationServiceStub').watchPosition = function() {
    // Do nothing for this test
  };

  this.render(hbs`
    {{#photo-search as |searchResults|}}
      {{#each searchResults as |result|}}
        <div class="test-result"></div>
      {{/each}}
    {{/photo-search}}
  `);

  this.$('.test-search-input').val('n').trigger('input');
  this.$('.test-search-input').val('ny').trigger('input');
  this.$('.test-search-input').val('nyc').trigger('input');
  wait().then(() => {
    assert.equal(searchCount, 1, 'Search should only be hit once');
    assert.equal(this.$('.test-result').length, 3, 'Three results show');
    this.get('flickrStub').search = originalSearch;
    this.get('locationServiceStub').watchPosition = originalWatchPosition;
    done();
  });
});

test('it triggers a new search when the location changes', function(assert) {
  assert.expect(2);

  let searchCount = 0;
  let initialSearchDone = assert.async();
  let searchAfterLocationChangeDone = assert.async();

  let originalSearch = this.get('flickrStub').search;
  this.get('flickrStub').search = function() {
    searchCount++;
    return Ember.RSVP.resolve([]);
  };

  let originalWatchPosition = this.get('locationServiceStub').watchPosition;
  this.get('locationServiceStub').watchPosition = function(callback) {
    this.callback = callback;
  };

  this.get('locationServiceStub').triggerCallbackToSimulateLocationChange = function() {
    Ember.run(() => {
      this.callback();
    });
  };

  this.render(hbs`{{photo-search}}`);

  this.$('.test-search-input').val('empire').trigger('input');

  wait().then(() => {
    assert.equal(searchCount, 1, 'Search has happened once');
    initialSearchDone();
  });

  wait().then(() => {
    this.get('locationServiceStub').triggerCallbackToSimulateLocationChange();
  });

  wait().then(() => {
    assert.equal(searchCount, 2, 'Search has happened twice');
    this.get('flickrStub').search = originalSearch;
    this.get('locationServiceStub').watchPosition = originalWatchPosition;
    this.get('locationServiceStub').triggerCallbackToSimulateLocationChange = null;
    searchAfterLocationChangeDone();
  });

});
